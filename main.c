#include <gtk/gtk.h>
#include <glib.h>

#define SEG_MAIN "Main"
#define OPT_TIME "time"
#define SEG_PIC "Pic"
#define OPT_PIC_NUM "picture_number"
#define OPT_PIC_LST "picture_list"

GtkWidget *window = NULL;
gint pic_num;

GArray* bitmap= NULL;
GArray* pixmap= NULL;


void main_quit()
{
    int i;
    for(i=0;i<pic_num;++i)
    {
        g_object_unref(g_array_index(bitmap, GdkBitmap*, i));
        g_object_unref(g_array_index(pixmap, GdkPixmap*, i));
    }

    g_array_free(bitmap, FALSE);
    g_array_free(pixmap, FALSE);

    printf("quit\n");
    gtk_main_quit();
}

gboolean window_click(GtkWidget *widget, GdkEventButton *event, GdkWindowEdge edge)  
{  
    if (event->button == 1)  
    {  
        gtk_window_begin_move_drag(GTK_WINDOW(window), event->button, event->x_root, event->y_root, event->time);  
    }
    else if(event->button== 3)
    {
        main_quit();
    }
    return FALSE;
}
gboolean timer_callback()
{
    static int idx= 1;
    ++idx;
    idx= idx==pic_num? 0: idx;
    gtk_widget_shape_combine_mask(window, g_array_index(bitmap, GdkBitmap*, idx), 0, 0);
    gdk_window_set_back_pixmap(window->window, g_array_index(pixmap, GdkPixmap*, idx), FALSE);
 
    gtk_widget_queue_draw(window);
    return TRUE;
}

int main(int argc, char *argv[])
{
    guint timer;
    gint tmr_timeout;
    gint win_w, win_h;
    gchar** pic_files;
    GdkPixbuf *pixbuf= NULL;
    GdkBitmap *bitmap1= NULL;
    GdkPixmap *pixmap1= NULL;

    bitmap= g_array_new(FALSE, FALSE, sizeof(GdkBitmap*));
    pixmap= g_array_new(FALSE, FALSE, sizeof(GdkPixmap*));
    
    gtk_init(&argc, &argv);
    GKeyFile* config= g_key_file_new();

    if(2== argc)
    {
        g_key_file_load_from_file(config, argv[1], 0, NULL);
    }
    else
    {
        g_key_file_load_from_file(config, "./Puppy.ini", 0, NULL);
    }
    tmr_timeout= g_key_file_get_integer(config, SEG_MAIN, OPT_TIME, NULL);
    if(0== tmr_timeout)
    {
        printf("Error: config file.");
        printf(OPT_TIME);
        printf("\n");
        return FALSE;
    }

    pic_num= g_key_file_get_integer(config, SEG_PIC, OPT_PIC_NUM, NULL);
    if(0== pic_num)
    {
        printf("Error: config file.\n");
        printf(OPT_PIC_NUM);
        printf("\n");
        return FALSE;
    }

    gsize get_file_num;
    pic_files= g_key_file_get_string_list(config, SEG_PIC, OPT_PIC_LST, &get_file_num, NULL);
    if(get_file_num!= pic_num)
    {
        printf("Error: config file.\n");
        printf(OPT_PIC_LST);
        printf("\n");
        return FALSE;
    }

    for(int i=0;i<pic_num;++i)
    {
        pixbuf= gdk_pixbuf_new_from_file(pic_files[i], NULL);
        gdk_pixbuf_render_pixmap_and_mask(pixbuf, &pixmap1, &bitmap1, 128);
        g_array_append_val(pixmap, pixmap1);
        g_array_append_val(bitmap, bitmap1);
        g_object_unref(pixbuf);
    }

    g_key_file_free(config);
     
 
    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_decorated(GTK_WINDOW(window), FALSE);         // 设置无边框

    gdk_pixmap_get_size(g_array_index(pixmap, GdkPixmap*, 0), &win_w, &win_h);
//    gtk_window_set_default_size(GTK_WINDOW(window), 240, 240);
    gtk_window_resize(GTK_WINDOW(window), win_w, win_h);
//    gtk_window_set_resizable(GTK_WINDOW(window), FALSE);
//    gtk_window_set_type_hint (GTK_WINDOW(window),GDK_WINDOW_TYPE_HINT_DESKTOP); 
//    gtk_window_set_keep_above(GTK_WINDOW(window), TRUE);
    gtk_widget_set_app_paintable(window, TRUE);
    gtk_widget_realize(window);

    gtk_widget_shape_combine_mask(window, g_array_index(bitmap, GdkBitmap*,0), 0, 0);             // 设置透明蒙板
    gdk_window_set_back_pixmap(window->window, g_array_index(pixmap, GdkPixmap*, 0), FALSE);             // 设置窗口背景
 
    timer= g_timeout_add(tmr_timeout, (GSourceFunc)timer_callback, NULL);

    gtk_widget_add_events(window, GDK_BUTTON_PRESS_MASK);

    g_signal_connect(G_OBJECT(window), "button-press-event", G_CALLBACK(window_click), NULL);
    g_signal_connect_swapped(G_OBJECT(window),"destroy",G_CALLBACK(main_quit), NULL);
    gtk_widget_show_all(window);
    gtk_main();
    return TRUE;
} 
